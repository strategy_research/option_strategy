#include "stdafx.h"
#include "Order.h"
#include "../ctpapi/ThostFtdcUserApiDataType.h"
#include "../ctpapi/ThostFtdcUserApiStruct.h"
#include "string.h"
#include <iostream>

extern char* user_id;
extern char* broker_id;

Order::Order()
{
	
}


Order::~Order()
{
}

CThostFtdcInputOrderField Order::Send_Limit_Order(char* inst, double price,
	int num, int direction, int open_close_flag,int orderref)
{
	CThostFtdcInputOrderField order;
	memset(&order, 0, sizeof(order));
	strcpy_s(order.BrokerID, broker_id);
	strcpy_s(order.InvestorID, user_id);
	strcpy_s(order.InstrumentID, inst);
	order.LimitPrice=price;
	order.VolumeTotalOriginal = num;
	order.VolumeCondition = THOST_FTDC_VC_AV;
	order.ContingentCondition = THOST_FTDC_CC_Immediately;
	order.TimeCondition = THOST_FTDC_TC_GFD;
	char char_orderref[13]="";
	_itoa_s(orderref++, char_orderref,13,10);
	strcpy_s(order.OrderRef, char_orderref);
	order.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
	if (direction > 0)
	{
		order.Direction = THOST_FTDC_D_Buy;;
	}
	else
	{
		order.Direction = THOST_FTDC_D_Sell;;
	}
	if (open_close_flag > 0)
	{
		order.CombOffsetFlag[0] = THOST_FTDC_OF_Open;
	}
	else
	{
		//strcpy_s(order.CombOffsetFlag, "1");
		order.CombOffsetFlag[0] = THOST_FTDC_OF_Close;
	}
	strcpy_s(order.CombHedgeFlag,"1");
	order.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
	order.IsAutoSuspend = 0;
	order.MinVolume = 0;
	return order;
}

CThostFtdcInputOrderActionField Order::Make_Action_Order(TThostFtdcActionFlagType Action)
{
	CThostFtdcInputOrderActionField order;
	memset(&order, 0, sizeof(order));
	order.ActionFlag = Action;
	strcpy_s(order.BrokerID, broker_id);
	strcpy_s(order.InstrumentID, InstrumentID);
	strcpy_s(order.InvestorID, user_id);
	strcpy_s(order.OrderSysID, OrderSysID);
	strcpy_s(order.OrderRef, OrderRef);
	order.SessionID = SessionID;
	strcpy_s(order.ExchangeID, ExchangeID);
	order.RequestID = RequestID;
	order.FrontID = FrontID;
	return order;
}

void Order::Get_Order_Information(CThostFtdcOrderField pOrder)
{
	strcpy_s(ExchangeID, pOrder.ExchangeID);
	strcpy_s(InstrumentID, pOrder.InstrumentID);
	strcpy_s(OrderRef, pOrder.OrderRef);
	strcpy_s(OrderSysID, pOrder.OrderSysID);

	SessionID = pOrder.SessionID;
	RequestID = pOrder.RequestID;
	FrontID = pOrder.FrontID;
	LimitPrice = pOrder.LimitPrice;

}
