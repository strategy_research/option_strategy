// ConsoleApp_cpp.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "Order.h"
#include"Instrument.h"
#include "Quote.h"
#include "Trade.h"
#include "string.h"
#include <iostream>
#include <stack>
#include <queue>
#include <Windows.h>
using namespace std;


#pragma region 自定义函数声明
int	Get_Minutes(char* time);
int Get_Seconds(char* time);

TThostFtdcDirectionType Inverse_Direction(TThostFtdcDirectionType direction);
TThostFtdcCombOffsetFlagType* Inverse_Offset(TThostFtdcCombOffsetFlagType* offset);

int Get_Option_Strike(char* ins);

#pragma endregion

CThostFtdcMdApi* md;
CThostFtdcTraderApi* api;
Quote* q = new Quote();
Trade* t = new Trade();

Instrument* option_1 =new Instrument();
Instrument* option_2=new Instrument();
Instrument* future_1 = new Instrument();

int nReq = 0;
int nReq_Account = 0;
int nReq_Order = 0;
int nTick = 0;

double tp = 0;
bool submit_cancel_order = false;
bool submit_trade_order = false;
Order CanCancel_Order;

char* Cancel_Ins;
TThostFtdcSessionIDType Cancel_session;
TThostFtdcOrderSysIDType Cancel_sysid;
char* Cancel_exchangeid;
char* Cancel_orderref;
int Cancel_minute;
int Cancel_second;
TThostFtdcFrontIDType Cancel_frontid;
TThostFtdcRequestIDType Cancel_requestid;
double Cancel_price;

//char* broker_id = "6090";
//char* user_id = "96400";
//char* md_front = "tcp://116.228.171.216:61213";
//char* trade_front = "tcp://116.228.171.216:61205";
//char* pass = "120066";

//中信期货信息-不可做期货
char* broker_id = "1111";
char* user_id = "60022026";
char* pass = "125213";
char* md_front = "tcp://115.238.108.173:41215";
char* trade_front = "tcp://115.238.108.173:41207";

char* ins[] = { "IO1406-C-2150", "IO1406-C-2100", "IF1406" };

enum  State_Mac
{
	init,
	watching,
	ordering,
	position,
	oneleg
};

State_Mac current_state = init;
stack<State_Mac> state_list;

int _tmain(int argc, _TCHAR* argv[])
{
	api = CThostFtdcTraderApi::CreateFtdcTraderApi("",false);
	md = CThostFtdcMdApi::CreateFtdcMdApi("", false);
	api->RegisterSpi((CThostFtdcTraderSpi*)t);
	md->RegisterSpi((CThostFtdcMdSpi*)q);

	api->SubscribePrivateTopic(THOST_TERT_QUICK);
	api->SubscribePublicTopic(THOST_TERT_QUICK);

	md->RegisterFront(md_front);
	api->RegisterFront(trade_front);
	
	md->Init();
	api->Init();

	api->Join();
	return 0;
	return 0;
}

#pragma region 交易登陆及资金查询
//注册交易前置机之后进行账户登陆
void Trade::OnFrontConnected()
{
	CThostFtdcReqUserLoginField f;
	memset(&f, 0, sizeof(f));
	strcpy_s(f.BrokerID, broker_id);
	strcpy_s(f.UserID, user_id);
	//cout << "请输入账户密码: " << endl;
	//cin >> pass;
	strcpy_s(f.Password, pass);
	api->ReqUserLogin(&f, ++nReq);
}

//交易登陆回调
void Trade::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << "trade:" << pRspInfo->ErrorMsg << endl;
	if (pRspInfo->ErrorID == 0)
	{
		CThostFtdcSettlementInfoConfirmField f;
		memset(&f, 0, sizeof(f));
		api->ReqSettlementInfoConfirm(&f, ++nReq);
		cout << "longin success!" << endl;
	}
	else
	{
		cout << pRspInfo->ErrorMsg << endl;
	}

	const char* tradingday;
	tradingday = md->GetTradingDay();
	cout << "the trading day is :" << tradingday << endl;

	CThostFtdcQryTradingAccountField account;
	memset(&account, 0, sizeof(account));
	strcpy_s(account.InvestorID, user_id);
	strcpy_s(account.BrokerID, broker_id);

	api->ReqQryTradingAccount(&account, nReq_Account);
	nReq_Account++;
}

//账户资金查询
void Trade::OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount,
	CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << "账户当前资金为:  " << pTradingAccount->Balance << endl;
	cout << "  " << endl;

	current_state = init;
	state_list.push(current_state);
	
}

void Trade::OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	//cout << pRspInfo->ErrorID << endl;
	//cout << pRspInfo->ErrorMsg << endl;
	if (pRspInfo!=NULL && pRspInfo->ErrorID!=0)
	{
		cout << pRspInfo->ErrorMsg << endl;
	}
	else
	{
		if (pInstrument!=NULL)
		{
			cout << "Instrument: " << pInstrument->InstrumentID << " Minimal Tick is: " << pInstrument->PriceTick << endl;
		}	
	}
	
}

#pragma endregion

#pragma region 行情登陆及订阅行情
//注册行情前置机后进行用户注册
void Quote::OnFrontConnected()
{
	cout << "行情前置机登陆成功! " << endl;
	CThostFtdcReqUserLoginField f;
	memset(&f, 0, sizeof(f));
	strcpy_s(f.BrokerID, "6090");
	strcpy_s(f.UserID, "96400");
	strcpy_s(f.Password, "120066");
	md->ReqUserLogin(&f, ++nReq);

}

//用户注册后进行行情订阅
void Quote::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,
	CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << "用户登陆成功" << *pRspInfo->ErrorMsg << endl;
	
	int subres = 9999;
	subres = md->SubscribeMarketData(ins, sizeof(ins)/sizeof(ins[0]));
	cout << "订阅返回结果： " << subres << endl;
	//初始品种
	option_1->Init_Name(ins[0]);
	option_2->Init_Name(ins[1]);
	future_1->Init_Name(ins[2]);
	option_1->strikeprice = Get_Option_Strike(option_1->ID);
	option_2->strikeprice = Get_Option_Strike(option_1->ID);

	//进行品种查询
	Sleep(3000);
	CThostFtdcQryInstrumentField instrument;
	memset(&instrument, 0, sizeof(instrument));
	instrument.ExchangeID[0] = THOST_FTDC_EIDT_CFFEX;
	strcpy_s(instrument.InstrumentID, "IO1406-C-2150");
	int result = api->ReqQryInstrument(&instrument, ++nReq);
	cout << "合约查询结果： " << result;
}

//品种订阅通知
void Quote::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument,
	CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << "行情订阅成功，订阅品种为: " << pSpecificInstrument->InstrumentID << endl;
}

void Quote::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << *pRspInfo->ErrorMsg << endl;
}

#pragma endregion

#pragma region 交易事件处理:挂单,成交,撤单

void Trade::OnRspOrderInsert(CThostFtdcInputOrderField *pInputOrder,
	CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << pRspInfo->ErrorMsg << endl;
}

//挂单回报
void Trade::OnRtnOrder(CThostFtdcOrderField *pOrder)
{
	if (pOrder->OrderStatus == THOST_FTDC_OST_NoTradeQueueing)
	{
		current_state = ordering;
		state_list.push(current_state);
		CanCancel_Order.Get_Order_Information(*pOrder);

		char* temp = pOrder->InsertTime;
		string current_time(temp);
		string minutes=current_time.substr(3, 2);
		string seconds = current_time.substr(6, 2);
		Cancel_minute = atoi(minutes.c_str());
		Cancel_second = atoi(seconds.c_str());
		
		pOrder->InsertTime;
		cout << "在时间:  " << pOrder->InsertTime << "  收到挂单:  " << pOrder->InstrumentID << "     " << pOrder->LimitPrice << endl;
	}
	else if (pOrder->OrderStatus == THOST_FTDC_OST_Canceled)
	{
		cout << "撤单成功: " << pOrder->InstrumentID << endl;
		submit_cancel_order = false;
		submit_trade_order = false;
		state_list.pop();
		current_state = state_list.top();

	}
	else if (pOrder->OrderStatus == THOST_FTDC_OST_AllTraded)
	{
		submit_trade_order = false;
		cout << "报单全部成交: " << pOrder->InstrumentID << endl;
		if (strcmp( pOrder->InstrumentID,option_1->ID))
		{
			if (pOrder->Direction == THOST_FTDC_D_Buy)
			{
				option_1->lasttrade_dir = pOrder->VolumeTraded;
			}
			else
			{
				option_1->lasttrade_dir = (-1)*pOrder->VolumeTraded;
			}
		}
		else if (strcmp(pOrder->InstrumentID, option_2->ID))
		{
			if (pOrder->Direction == THOST_FTDC_D_Buy)
			{
				option_2->lasttrade_dir = pOrder->VolumeTraded;
			}
			else
			{
				option_2->lasttrade_dir = (-1)*pOrder->VolumeTraded;
			}
		}
	}
	else if (pOrder->OrderStatus == THOST_FTDC_OST_PartTradedQueueing)
	{
		cout << "部分成交: " << pOrder->InstrumentID << endl;
	}

}
//成交回报
void Trade::OnRtnTrade(CThostFtdcTradeField *pTrade)
{
	cout << "取得成交回报,成交品种为: " << pTrade->InstrumentID << "  成交价格为： " << pTrade->Price << endl;
	if (strcmp(pTrade->InstrumentID, option_1->ID) == 0)
	{
		cout << "Option 1 trade: " << pTrade->Price << endl;
		option_1->lasttrade = pTrade->Price;

		current_state = oneleg;
		state_list.push(current_state);
		Order order;
		nReq_Order++;
		if (pTrade->Direction == THOST_FTDC_D_Buy)
		{
			if (pTrade->OffsetFlag == THOST_FTDC_OF_Open)
			{
				api->ReqOrderInsert(&order.Send_Limit_Order(option_2->ID, 
					option_2->bidprice - 5*tp, 3, -1, 1, nReq_Order), nReq_Order);
			}
			else
			{
				api->ReqOrderInsert(&order.Send_Limit_Order(option_2->ID,
					option_2->bidprice - 5*tp, 3, -1, 0, nReq_Order), nReq_Order);
			}
			
		}
		else
		{
			if (pTrade->OffsetFlag == THOST_FTDC_OF_Open)
			{
				api->ReqOrderInsert(&order.Send_Limit_Order(option_2->ID, 
					option_2->askprice + tp, 3, 1, 1, nReq_Order), nReq_Order);
			}
			else
			{
				api->ReqOrderInsert(&order.Send_Limit_Order(option_2->ID,
					option_2->askprice + tp, 3, 1, 0, nReq_Order), nReq_Order);
			}
		}

	}
	else if (strcmp(pTrade->InstrumentID, option_2->ID) == 0)
	{
		cout << "Option 2 trade: " << pTrade->Price << endl;
		option_2->lasttrade = pTrade->Price;

		current_state = position;
		state_list.push(current_state);
		Order order;
		nReq_Order++;
		if (pTrade->Direction == THOST_FTDC_D_Buy)
		{
			//api->ReqOrderInsert(&order.Send_Limit_Order(future_1->ID, future_1->askprice, 1, -1, 1, nReq_Order), nReq_Order);
		}
		else
		{
			//api->ReqOrderInsert(&order.Send_Limit_Order(future_1->ID, future_1->askprice, 1, -1, 0, nReq_Order), nReq_Order);
		}
		cout << "****************************************" << endl;
		cout << "组合成本为: " << (option_1->lasttrade*option_1->lasttrade_dir) -
			(option_2->lasttrade*option_2->lasttrade_dir) << endl;
	}
\
}

void Trade::OnErrRtnOrderAction(CThostFtdcOrderActionField *pOrderAction, CThostFtdcRspInfoField *pRspInfo)
{
	cout << "输出撤单回报信息" << endl;
	cout << pRspInfo->ErrorID << endl;
	cout << pRspInfo->ErrorMsg << endl;
	
}

void Trade::OnRspOrderAction(CThostFtdcInputOrderActionField *pInputOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << "输出RSP撤单回报信息" << endl;
	cout << pRspInfo->ErrorID << endl;
	cout << pRspInfo->ErrorMsg << endl;
	submit_cancel_order = false;
	state_list.pop();
	current_state = state_list.top();

}
#pragma endregion

//策略主体逻辑
void Quote::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData)
{
	cout << "当前策略状态为: " << current_state << endl;
	nTick++;
	if (nTick>10)
	{
		current_state = watching;
		state_list.push(current_state);
	}

	//cout << "品种: " << pDepthMarketData->InstrumentID << "  最新价格: "<<pDepthMarketData->LastPrice <<endl;
	if (strcmp(pDepthMarketData->InstrumentID, option_1->ID) == 0)
	{
		option_1->askprice = pDepthMarketData->AskPrice1;
		option_1->bidprice = pDepthMarketData->BidPrice1;
		option_1->lastprice = pDepthMarketData->LastPrice;
		//cout << pDepthMarketData->InstrumentID << " " << option_1->askprice << "  " << option_1->bidprice << endl;
	}
	else if (strcmp(pDepthMarketData->InstrumentID, option_2->ID) == 0)
	{
		option_2->askprice = pDepthMarketData->AskPrice1;
		option_2->bidprice = pDepthMarketData->BidPrice1;
		option_2->lastprice = pDepthMarketData->LastPrice;
		//cout << pDepthMarketData->InstrumentID << " " << option_2->askprice << "  " << option_2->bidprice << endl;
	}
	else
	{
		future_1->askprice = pDepthMarketData->AskPrice1;
		future_1->bidprice = pDepthMarketData->BidPrice1;
		future_1->lastprice = pDepthMarketData->LastPrice;
		//cout << pDepthMarketData->InstrumentID << " " << future_1->askprice << "  " << future_1->bidprice << endl;
	}
	//垂直套利
	double m1_profit = option_2->strikeprice - option_1->strikeprice + option_2->bidprice - option_1->askprice;
	double m2_profit = option_1->strikeprice - option_2->strikeprice + option_1->bidprice - option_2->askprice;
	//买入期货---卖空期权1---买入期权2
	//double m2_profit = 2150 - future_1->askprice + 3 * option_1->bidprice - 3 * option_2->askprice;

	if (future_1->askprice > 0 && option_1->askprice > 0 && option_2->askprice > 0 && current_state != position)
	{
		cout << "Sell 2 Buy 1: profit point is: " << m1_profit << endl;
		cout << "Buy 2 Sell 1: profit point is: " << m2_profit << endl;
	}
	double profit = 0;
	if (current_state == position)
	{
	    profit =  (option_1->lasttrade - option_1->askprice)*option_1->lasttrade_dir +
			 (option_2->bidprice - option_2->lasttrade)*option_2->lasttrade_dir;
		cout << "当前利润为:  " << profit << endl;

		if (profit >= 2)
		{
			Order order;
			int result = 999;
			nReq_Order++;
			if (option_1->lasttrade_dir > 0)
			{
				result = api->ReqOrderInsert(&order.Send_Limit_Order(option_1->ID, 
					option_1->bidprice - tp, option_1->lasttrade_dir, -1, 0, nReq_Order), nReq_Order);
				submit_trade_order = true;
			}
			else
			{
				result = api->ReqOrderInsert(&order.Send_Limit_Order(option_1->ID, 
					option_1->askprice + tp, -1 * option_1->lasttrade_dir, 1, 0, nReq_Order), nReq_Order);
				submit_trade_order = true;
			}
			
		}
	}

	if (m2_profit > 5 && current_state == watching&& submit_trade_order == false)
	{
		Order order;
		int result = 999;
		nReq_Order++;
		result = api->ReqOrderInsert(&order.Send_Limit_Order(option_1->ID, option_1->askprice-tp, 3, -1, 1, nReq_Order), nReq_Order);
		cout << option_1->ID << " 开始报单，当前状态转为ordering " << endl;
		submit_trade_order = true;
		
	}
	else if (m1_profit > 5 && current_state == watching && submit_trade_order == false)
	{
		Order order;
		int result = 999;
		nReq_Order++;
		result = api->ReqOrderInsert(&order.Send_Limit_Order(option_1->ID, option_1->bidprice + tp, 3, 1, 1, nReq_Order), nReq_Order);

		cout << option_1->ID << " 开始报单，当前状态转为ordering " << endl;
		submit_trade_order = true;

	}

	if (current_state == ordering)
	{
		string update_time = string(pDepthMarketData->UpdateTime);
		string minute_temp = update_time.substr(3, 2);
		string second_temp = update_time.substr(6, 2);
		int tick_minute = atoi(minute_temp.c_str());
		int tick_second = atoi(second_temp.c_str());
		int timegap = (tick_minute - Cancel_minute) * 60 + tick_second - Cancel_second;
		if (strcmp( pDepthMarketData->InstrumentID,Cancel_Ins)==0 
			&& pDepthMarketData->AskPrice1<Cancel_price
			&& submit_cancel_order==false)
		{
			CThostFtdcInputOrderActionField order;
			order = CanCancel_Order.Make_Action_Order(THOST_FTDC_AF_Delete);

			int result = api->ReqOrderAction(&order, nReq_Order);
			cout << "当前卖价位: " << pDepthMarketData->AskPrice1 << endl;
			cout << "撤单挂价为: " << Cancel_price << endl;
			if (result == 0)
			{
				submit_cancel_order = true;
			}
			else
			{
				submit_cancel_order = false;
				cout << "撤单失败,错误代码为: " << result << endl;
			}
		}

	}
}

int Get_Option_Strike(char* ins)
{
	string name = string(ins);
	string	temp = name.substr(9, 4);
	return atoi(temp.c_str());
}

TThostFtdcDirectionType Inverse_Direction(TThostFtdcDirectionType direction)
{
	if (direction == THOST_FTDC_D_Buy)
	{
		return THOST_FTDC_D_Sell;
	}
	else
	{
		return THOST_FTDC_D_Buy;
	}
}

TThostFtdcCombOffsetFlagType* Inverse_Offset(TThostFtdcCombOffsetFlagType* offset)
{

	return 0;
}

int	Get_Minutes(char* time)
{
	string current_time(time);
	string minutes = current_time.substr(3, 2);
	return atoi(minutes.c_str());

}

int Get_Seconds(char* time)
{
	string current_time(time);
	string second = current_time.substr(6, 2);
	return atoi(second.c_str());
}
