#pragma once
#include "../ctpapi/ThostFtdcUserApiDataType.h"
#include "../ctpapi/ThostFtdcUserApiStruct.h"
class Order
{
public:
	Order();
	~Order();

	///合约代码
	TThostFtdcInstrumentIDType	InstrumentID;
	///报单引用
	TThostFtdcOrderRefType	OrderRef;
	///买卖方向
	TThostFtdcDirectionType	Direction;
	///组合开平标志
	TThostFtdcCombOffsetFlagType	CombOffsetFlag;
	///组合投机套保标志
	TThostFtdcCombHedgeFlagType	CombHedgeFlag;
	///价格
	TThostFtdcPriceType	LimitPrice;
	///数量
	TThostFtdcVolumeType	VolumeTotalOriginal;
	///会话ID
	TThostFtdcSessionIDType	SessionID;
	///报单编号
	TThostFtdcOrderSysIDType	OrderSysID;
	///报单的前置机编号
	TThostFtdcFrontIDType FrontID;

	TThostFtdcExchangeIDType ExchangeID;
	TThostFtdcRequestIDType RequestID;

	CThostFtdcInputOrderField Send_Limit_Order(char* inst, double price, int num, int direction, int open_close_flag,int orderref);
	
	CThostFtdcInputOrderActionField Make_Action_Order(TThostFtdcActionFlagType Action);

	void Get_Order_Information(CThostFtdcOrderField pOrder);
};

